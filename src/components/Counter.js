import styled from 'styled-components';
import {useDispatch, useSelector} from 'react-redux';
import {decrement, increment} from '../store';

const Button = styled.button`
  box-sizing: border-box;
  flex-shrink: 0;
  width: 160px;
  height: 61px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 16px;
  background-color: #ffffff;
  overflow: visible;
  border-radius: 24px;
  border: 1px black;
  white-space: pre;
  font-weight: 700;
  font-family: "Inter-Bold",
  "Inter", sans-serif;
  color: #000000;
  font-size: 24px;
  letter-spacing: 0px;
  line-height: 1.2;
`
const Count = styled.p`
  flex-shrink: 0;
  width: 160px;
  height: 71px;
  white-space: pre-wrap;
  word-wrap: break-word;
  word-break: break-word;
  font-weight: 700;
  font-family: "Inter-Bold", "Inter", sans-serif;
  color: #8cd6bd;
  font-size: 64px;
  letter-spacing: 0px;
  line-height: 1.2;
  text-align: center;
`
const Section = styled.section`
  width: 390px;
  height: 844px;
  background-color: #004761;
  overflow: hidden;
  margin: 0 auto;
`

const Container = styled.div`
  width: 160px;
  height: 400px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  overflow: visible;
  margin: 222px 115px;`

export const Counter = () => {
    const counter = useSelector(state => state.count)
    const dispatch = useDispatch()
    const handleIncrement = () => {
      dispatch(increment())
    }
    const handleDecrement = () => {
        dispatch(decrement())
    }

    return (
        <Section>
            <Container>
                <Button onClick={handleDecrement}> -1 </Button>
                <Count data-testid='count'>{counter}</Count>
                <Button onClick={handleIncrement}> +1 </Button>
            </Container>

        </Section>
    )
}