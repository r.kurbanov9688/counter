import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {Counter} from './Counter';
import {Provider} from 'react-redux';
import {store} from '../store';

let counter
let increaseCount
let decrementCount

beforeEach(() => {
    render(
        <Provider store={store}>
            <Counter/>
        </Provider>
        )
    counter = screen.getByTestId('count')
    increaseCount = screen.getByRole('button', { name: '+1' })
    decrementCount = screen.getByRole('button', { name: '-1' })
})

it('should render a counter with value of 0', () => {
    expect(counter).toHaveTextContent('0')
})

it('should increase count when +1 button is clicked', () => {
    expect(counter).toHaveTextContent('0')
    userEvent.click(increaseCount)
    expect(counter).toHaveTextContent('1')
})

it('should decrease count when -1 button is clicked', () => {
    expect(counter).toHaveTextContent('1')
    userEvent.click(decrementCount)
    expect(counter).toHaveTextContent('0')
})

